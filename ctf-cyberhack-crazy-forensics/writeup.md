# [CyberhackCTF] Crazy Forensics

Hi hekers!, since our team is preparing for the Ekoparty CTF, we decided to participate in many CTF's around the world, so now, we test our skills in the CyberhackCTF, a group of indian ctf player that made an awesome event (with some infraestructure problem, but just details).

![cyberlogo](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/cyberhacklogo.png)

Today, I want to share the **Crazy Forensics** challenge writeup, that give us 450 pts.

![start](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/start.png)


The challenge begins by indicating the format and content of the flag `Cyberhack{DeletedDate_Time}`, which in this case is a specific date that we must find in the attached files.

## First Enumeration: Rabbit Hole

The challenge contain 3 file that must be analyzed (the `desc.txt` is just the description file), so we start by downloading these files.

![files](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/files.png)


First of all, I verified the correct file format write in the name of the file. THinking on this, we should have 1 zip file, 1 jpg image and mp4 video.

![types](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/file_tipes.png)

From here, I start to analyzing the jpg file in order to get some useful data. The original image was from an Indian movie called `Hera Pheri`, but it doesn't matter for our real pourpuse. 

![hera](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/hera_pheri.png)



Using binwalk tool, we can see that the file has an embeved PNG file.

![binwalk](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/binwalk.png)

Initially, I tryed using binwalk, but, it doesn't extract the embeved image as I expected, so I use a powerfull tool called [Stegoveritas](https://github.com/bannsec/stegoVeritas) and it succesfully extract the image. 

![stego](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/output_stegoveritas.png)


Unfortunately, tha was just a rabbit hole, so I continue analyzing the other files.

![rabbit](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/rabbit_hole.png)


## Frame by Frame

Now, I tryed analizing the video, so the first approach was watch the whole video. THis video, was related to `Hera Phera` and my first impression was that certain frames from the video were modified, because it show some noise in the flow of the video. 

After two times watching this, I noticed that in an specific frame of the video, some word are printed on the screen, so, I tryed to follow the videos frame by frame and finally got something.

![video](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/video-pass.png)


We got it: `21_dinn_meinn_paisaa_doublee`

At thiss moment, I didn't know what was the real utility of this, but of course, I save this in case it was useful later.

## Finding the Erased Date

Now, we focus our attention on the `challenge.zip` file. When I tryed uncompress this using `unzip` tool, the ouput rise an exception because we are using a different version of PK compat.

```BASH
h4tt0r1@kitsun3:~/Documentos/ctf-cyberhack/forensic/crazy-forensics$ unzip challange.zip 
Archive:  challange.zip                                                                                
   skipping: $INECDME.exe            need PK compat. v5.1 (can do v4.6)
``` 

So, we tryed the same using 7z and it work, but a password is required to continue. Now, we can use the indian phrase found in the last step.

![7z](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/7z_extract.png)

Cool, now, it's time to play with the binary file. First, the cat command reveal us that it has just a system windows path and some extra bytes for the file header.

![cat](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/cat_exe.png)

Also, using radare2, I tried to see if the file has a consistant content, but nothing usefull could be found.

![r2](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/radare_output.png)


Then, in order to understand why the file start with a `$`, I found a little documentation about  `Recycler.bin` files in windows [here](https://www.blackbagtech.com/blog/examining-the-windows-10-recycle-bin/)

This post, explain that `Files starting with $I are essentially the metadata for the particular file that was deleted`, so, we just have the metadata from a PE file. The same post, show us, what type of metada we can obtain from this type of files and the field `Date/Time Deleted` can be found of this file.

Some minutes later, I finally get a useful tool that can be restore this deleted time from the file metadata. The `$I Parse` found [here](https://df-stream.com/recycle-bin-i-parser/), need to be excecute in a windows environment, so I use my own windows VM for this purpose.

![parse](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-cyberhack-crazy-forensics/parseexe.png)

And finally we got the deleted time `05-15-2019 15:54:51`, but remember the specified format at the start of this challenge. Now the final flag is:

`Cyberhack{05/15/2019_15:54:51UTC}`

Greetings!





