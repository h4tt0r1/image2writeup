# [ConvidCTF - Lorem Ipsum | Base64 o que? | Lorenzo]

Buenas heckers!, el fin de semana pasado estuvimos jugando con el equipo de Cntr0llz el evento CTF Convid ***Made In Chile***, el cual tuvo bastantes desafios entretenidos y que estaban a la altura de una buena competencia contra equipos como #alertot, #Dystopia y #jot-kiddies. Para buena suerte de nosotors, logramos un primer lugar despues de mas de 24 horas jugando sin descanso y bebidas Monster.

![gif_yay](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-convid/tenor.gif)

Por todo esto, decidimos realizar algunos de los writeup de la competencia y en particular, en este post les traigo los retos nombrados en el título de la publicación.

## Lorem Ipsum 

![start_1](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-convid/lorem/start.png)

Lorem Ipsum fue uno de los últimos retos que pudimos resolver. A pesar de solo tener 50 puntos, había un paso específico que no logramos sacar sino hasta al final de la competencia gracias a unas pequeñas hint de los admin.

El reto inicialmente muestra los típicos parrafos de lorem Ipsum en lenguaje latin, generados para rellenar texto inutil.

![text](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-convid/lorem/texto-lorem.png)

Como soy usuario de sublime, acostumbro a seleccionar los textos para ver si existe algo que no se vea a simple vista, lo cual, sublime permite visualizar de forma clara, en este caso, los espacios (representados por un punto) y tabulaciones (representados por una linea).

![white](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-convid/lorem/puwhitespace.png)

Inmediatamente recorde la herramienta Stegsnow que utilizamos en el CTF de Fireshell con mis compañeros hace un tiempo para resolver un reto similar. [Stegsnow](http://manpages.ubuntu.com/manpages/bionic/man1/stegsnow.1.html)(en su repo oficial), permite descifrar mensajes escondidos en los textos de este estilo, utilizando algo similar a **Whitespace Esolang**.

Para la utilización de stegsnow, solo basta con especificar la data comprimida y la contraseña para descifrar el texto.

```
h4tt0r1@kitsun3:~$ stegsnow -C -p "mipassword" default_text.txt

```

Desafortunadamente, la password que necesitabamos, fue lo mas complicado de obtener, pues requeria algo de guessing y suerte utilizando algun diccionario (me dijeron que con rockyou salia, pero luego de 1 hora corriendo mi script me di por vencido).

Finalmente, logré obtener la password luego de miles de intentos y obtener la flag final :)

![Flag](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-convid/lorem/flag.png)

Flag: **CL{l0r3m_1psum_d0l0r_s1t_fl4g}**

## ¿Base64 o que?


![start_2](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-convid/base64-o-que/start.png)

Este reto me trajo algo de nostalgia, cuando empecé a dedicarme a los CTF por el año 2016, hubo un writeup de [Fwhibbit](https://fwhibbit.es/) que me llamó mucho la atención, por lo que recordé una técnica utilizada para resolver este reto.

El string inicial que que nos dan es:

```
HyrjrxWsPOjhWODRTCwIsTMgxMqDUpSvfqckjwbIjYesxtmbjFecbqrbtMIxDbIXxXCMRgFtrcnykQgmHqcQLdxmXiMOTQtefRGwVxWKhWLKgQoyJHIMTxEWXEoOLvSnnDyspYhJBsluScGWLEenvLuxDumjXfTeDIOQpVwSbOKbdUhetDuWhsUPvpYsliXfJwgWugPe
```

Y si bien tiene pinta de un base64, al no poder decodearlo utilizando el típico comando de terminal (`base64 -d`), recurrí a una herramienta llamada [Stegb64](https://github.com/hecky/stegb64/), la cual permite descifrar esta variante.

![stegb64](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-convid/base64-o-que/stegb64.png)

Finalmente, utilizamos la herramienta tal cual nos dice la documentación, y obtenemos la flag :)

![flag2](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-convid/base64-o-que/flag.png)

flag: **CL{jur4b4_qu3_3r4_base64}**

## Lorenzo! vieni qui....

![start3](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-convid/lorenzo/start.png)

El tercer y último reto, es uno relacionado a la criptografía utilizada en la época de la Segunda Guerra Mundial. Inicialmente el reto muestra un par de números y letras que para mí era algo nunca antes visto. Como lo sugiere el reto, comence a estudiar un poco sobre los cifrados utilizados en esa epoca para enviar mensajes. Leyendo un poco en wikipedia, pude toparme con un cifrado que tenia un nombre similar al del reto, por lo que decidi mirarlo más de cerca.

![lorenzo_wiki](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-convid/lorenzo/wikipedia.png)

El código de Lorenz SZ40, fue un cifrado teletipo utilizado por máquinas de combate para el transporte de datos. Para asegurarme de que habia dado con el cifrado correcto, busque varios ejemplos de este cifrado, en el cual explicaban cada uno de los parametros puestos en la descripcion del reto. Especificamente, esta [Guia](https://es.qwe.wiki/wiki/Cryptanalysis_of_the_Lorenz_cipher) explica la mayor parte de lo que hay que saber, pero fue realmente de curioso, que en **cybershef** pudiese encontrar el decoder de lorenz.

Finalmente jugando con los parametors mediante prueba y error, logre obtener la flag final del reto.

![flag3](https://gitlab.com/h4tt0r1/image2writeup/-/raw/master/ctf-convid/lorenzo/flag.jpg)

flag: **CL{3ST4FU3F4C1LCU4ND0L4D3SCUBR1}**


Saludos !!
[@h4tt0r1](https://twitter.com/hackttori?lang=es)